import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Click Tab Akun'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Click Button Masuk Akun'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Click Link Daftar'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Nama'), [('nama') : 'Andreas'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Email'), [('email') : 'andreasmail'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Password'), [('password') : GlobalVariable.password_def], 
    FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Input NomorHP'), [('nomorHP') : '08549863496'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Kota'), [('kota') : 'Jogja'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Alamat'), [('alamat') : 'Jl. jalan sore'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/scroll to button Daftar'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Click Button Daftar Regist'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Error Invalid Email Format'), [:], FailureHandling.STOP_ON_FAILURE)

