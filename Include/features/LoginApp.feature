@Login @Smoke
Feature: Login
  As a user, I want to login to Secondhand app

  @loginInvalid @LOG01
  Scenario: LOG01 - User want to login without input email and password
    Given user click tab Akun
    And user click button Masuk di tab Akun
    Then user click button Masuk di tab Login
    And error message appear

  @loginInvalid @LOG02
  Scenario Outline: LOG02 - User want to login using incorrect credential
    Given user click tab Akun
    And user click button Masuk di tab Akun
    When user input incorrect email "<email>"
    And user input incorrect password "<password>"
    Then user click button Masuk di tab Login
    And user will failed to login

    Examples: 
      | email                | password  |
      | binarteam2@gmail.com | wrongPass |
      | wrongEmail@gmail.com | binar0202 |
      | wrongEmail@gmail.com | wrongPass |

  @loginValid @LOG03
  Scenario: LOG03 - User want to login using correct credential
    Given user click tab Akun
    And user click button Masuk di tab Akun
    And user input email "binarteam2@gmail.com"
    And user input password "binar0202"
    Then user click button Masuk di tab Login
    And user will redirected to Akun Saya
