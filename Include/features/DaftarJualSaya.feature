
@DaftarJualSaya
Feature: Daftar Jual Saya

	@DJS01 @positive
	Scenario: DJS01 - User wants to see all products they have published
	Given User successfully logged in
	When User go to Daftar Jual Saya page
	Then The user can see their published products

	@DJS02 @positive
	Scenario: DJS02 - User wants to see products which buyer interested with
	Given User log in using an account that have a product transaction
	When User go to Daftar Jual Saya page
	And User click on Diminati tab
	Then User can see the products that another user have bidded

	@DJS03 @positive
	Scenario: DJS03 - User wants to see their sold products
	Given User log in using an account that have a product transaction
	When User go to Daftar Jual Saya page
	And User click on Terjual tab
	Then User can see the products that have sold

	@DJS04 @negative
	Scenario: DJS04 - User wants to see their products, although they did not published any product before
	Given User logged in using an account that user have not published any product yet
	When User go to Daftar Jual Saya page
	Then No products to be shown on every page in daftar jual saya page





