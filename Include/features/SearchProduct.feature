@SearchProduct
Feature: Search Product

	@SP01
  Scenario: SP01 - User search category Elektronik
    Given user launch Secondhand App
    When user tap on Elektronik category button
    Then user will redirected to product page with Elektronik category
    
  @SP02
  Scenario: SP02 - User search category Komputer dan Aksesoris
    Given user launch Secondhand App
    When user tap on Komputer dan Aksesoris category button
    Then user will redirected to product page with Komputer dan Aksesoris category
        
  @SP03
  Scenario: SP03 - User search All category product
    Given user launch Secondhand App
    When user tap on Elektronik category button
    Then user tap on All category button
    Then user will redirected to product page with All category
           
  @SP04
  Scenario: SP04 - User search by keyword
    Given user launch Secondhand App
    When user search baju in search product
    Then user will redirected to product page with baju keyword
