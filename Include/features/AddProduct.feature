
@AddProduct
Feature: Add Product

  @positive @ADP01
  Scenario: ADP01 - User wants to add a product
    Given User successfully log in to the web
    When User Add a Product
    Then User will see their product successfully published

  @negative @ADP02
  Scenario: ADP02 - User wants to add product without enter products name
    Given User successfully log in to the web
    When User click Add Product button
    When User wants to add a product without enter product name
    Then User will see an error message appear to validate name field

  @negative @ADP03
  Scenario: ADP03 - User wants to add product but did not log in to the app
    Given App successfully opened
    When User click Add Product button
    Then User will be asked to log in
