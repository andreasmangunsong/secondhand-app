@EditProfile
Feature: Edit Profile

  @EP01
  Scenario: EP01 - Edit Profile Without Update Data
    Given user launch Binar Secondhand App
    When User login into the account
    Then User click on Edit Profile button
    Then User click edit profile without update data
    Then User get success verification pop up

  @EP02
  Scenario: EP02 - User edit profile
    Given user launch Binar Secondhand App
    When User login into the account
    Then User click on Edit Profile button
    Then User edit profile
    Then User get success verification pop up

  @EP03
  Scenario: EP03 - User update profile with one blank field
    Given user launch Binar Secondhand App
    When User login into the account
    Then User click on Edit Profile button
    Then User blank one of the field
    Then User get Error message
    

