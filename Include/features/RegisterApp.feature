#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Register @Smoke
Feature: Register
  As a user, I want to register to secondhand web

  @RegisterInValid @REG01
  Scenario: REG01 - User want to register, but all fields are empty
    Given user click tab Akun
    And user click button Masuk di tab Akun
    And user click link daftar
    Then user scroll to button daftar
    And user click button daftar
    And error message appear empty field

  @RegisterInValid @REG02
  Scenario: REG02 - User want to register using incorrect email format
    Given user click tab Akun
    And user click button Masuk di tab Akun
    And user click link daftar
    When user input Nama "andreas"
    And user input incorrect email format "andreasmail"
    And user input password "binar0202"
    And user input nomor HP "0876416843"
    And user input kota "Jogja"
    And user input alamat "Jl. i aja dulu"
    Then user scroll to button daftar
    And user click button daftar
    And error message appear invalid email

  @RegisterValid @REG03
  Scenario: REG03 - User want to register using correct credential
    Given user click tab Akun
    And user click button Masuk di tab Akun
    And user click link daftar
    When user input Nama "andreas"
    And user input random email "randomEmail"
    And user input password "binar0202"
    And user input nomor HP "0876416843"
    And user input kota "Jogja"
    And user input alamat "Jl. i aja dulu"
    Then user scroll to button daftar
    And user click button daftar
    And user will redirected to Akun Saya
