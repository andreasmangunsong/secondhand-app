
@DaftarTransaksi
Feature: Daftar Transaksi

	@DAF01
  Scenario: DAF01 - Users wants to see transaction of their products
    Given User login using an account that have product transaction
    When User click on Transaksi Menu
    Then User will be shown the products which have an interaction or transaction

  @DAF02
	Scenario: DAF02 - Users does not have a product interaction or transaction
    Given User login using an account that does not have a transaction
    When User click on Transaksi Menu
    Then User will be shown message that tells no product interaction or transaction at the moment

  @DAF03
	Scenario: DAF03 - User open Transaction menu but did not log in to the app
    Given Application Susccesfully opened, but user have not log in to the app
    When User click on Transaksi Menu
    Then User will get a message that ask them to log in