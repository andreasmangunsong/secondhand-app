Feature: Update Product
	User wants to edit a product they have published before

	@UpdateProduct @UPD01 @positive
  Scenario: UPD01 - User wants Update their Product with valid data
	Given user successfully log in to the app
	When user navigated to product detail view
	When user input product name with "Test Edittt"
	And user input product price with "456000"
	And user input product category
	And user input location
	And user input product description
	And user input product image
	And user click button perbarui
	Then Product Updated and user directed to product view

	@UpdateProduct @UPD02 @negative
  Scenario: UPD02 - User wants to update without enter any data
	Given user successfully log in to the app
	When user navigated to product detail view
	When user clear text on product name
	And user clear text on product price
	And user clear selected category
	And User clear text on location
	And user clear text on product description
	And user click button perbarui
	Then error message will appear to validate empty fields

