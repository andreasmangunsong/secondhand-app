package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.WebMobileDriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class DaftarTransaksi {

	@Given("User login using an account that have product transaction")
	public void user_login_using_an_account_that_have_product_transaction() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Login/User Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click on Transaksi Menu")
	public void user_click_on_Transaksi_Menu() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Transaksi/User Click Tab Transaksi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will be shown the products which have an interaction or transaction")
	public void user_will_be_shown_the_products_which_have_an_interaction_or_transaction() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Transaksi/Verify Transaction Content'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@Given("User login using an account that does not have a transaction")
	public void user_login_using_an_account_that_does_not_have_a_transaction() {
		Mobile.startApplication('Apk/app-release-second-hand-gcp.apk', true)
		WebUI.callTestCase(findTestCase('Samuel/Pages/Login/User Login 2'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will be shown message that tells no product interaction or transaction at the moment")
	public void user_will_be_shown_message_that_tells_no_product_interaction_or_transaction_at_the_moment() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Transaksi/Verify Empty Transaction'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@Given("Application Susccesfully opened, but user have not log in to the app")
	public void application_Susccesfully_opened_but_user_have_not_log_in_to_the_app() {
		Mobile.startApplication('Apk/app-release-second-hand-gcp.apk', true)
	}

	@Then("User will get a message that ask them to log in")
	public void user_will_get_a_message_that_ask_them_to_log_in() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Transaksi/Verify User did not log in to the app'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}
}
