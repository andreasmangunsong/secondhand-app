package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class SearchProduct {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("user launch Secondhand App")
	public void user_launch_Secondhand_App() {
		WebUI.callTestCase(findTestCase('Kastori/Launch App'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user tap on Elektronik category button")
	public void user_tap_on_Elektronik_category_button() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Feature/Click Elektronik'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user tap on Komputer dan Aksesoris category button")
	public void user_tap_on_Komputer_dan_Aksesoris_category_button() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Feature/Click Komputer dan Aksesoris'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user search baju in search product")
	public void user_search_baju_in_search_product() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Feature/Click Search'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Feature/Set Text Search'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("user tap on All category button")
	public void user_tap_on_All_category_button() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Feature/Click All Category'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user will redirected to product page with Elektronik category")
	public void user_will_redirected_to_product_page_with_Elektronik_category() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Feature/Verify Elektronik'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user will redirected to product page with Komputer dan Aksesoris category")
	public void user_will_redirected_to_product_page_with_Komputer_dan_Aksesoris_category() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Feature/Verify Komputer dan Aksesoris'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user will redirected to product page with All category")
	public void user_will_redirected_to_product_page_with_All_category() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Feature/Verify All Category'), [:], FailureHandling.STOP_ON_FAILURE)
		
	}
	@Then("user will redirected to product page with baju keyword")
	public void user_will_redirected_to_product_page_with_baju_keyword() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Search Feature/Verify Search Keyword'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}
}
