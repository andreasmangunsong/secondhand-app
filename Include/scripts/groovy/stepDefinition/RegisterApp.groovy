package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class RegisterApp {
	@Given("user click link daftar")
	public void user_click_link_daftar() {
		Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Click Link Daftar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input Nama {string}")
	public void user_input_Nama(String string) {
		Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Nama'), [('nama') : 'Andreas'], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("user input random email {string}")
	public void user_input_random_email(String email) {
		WebUI.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Random Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input nomor HP {string}")
	public void user_input_nomor_HP(String string) {
		Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Input NomorHP'), [('nomorHP') : '08549863496'], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input kota {string}")
	public void user_input_kota(String string) {
		Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Kota'), [('kota') : 'Jogja'], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input alamat {string}")
	public void user_input_alamat(String string) {
		Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Alamat'), [('alamat') : 'Jl. jalan sore'], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user scroll to button daftar")
	public void user_scroll_to_button_daftar() {
		Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/scroll to button Daftar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click button daftar")
	public void user_click_button_daftar() {
		Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Click Button Daftar Regist'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("error message appear empty field")
	public void error_message_appear_empty_field() {
		Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Error Empty Nama'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@When("user input incorrect email format {string}")
	public void user_input_incorrect_email_format(String string) {
		Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Input Email'), [('email') : 'andreasmail'], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("error message appear invalid email")
	public void error_message_appear_invalid_email() {
		Mobile.callTestCase(findTestCase('Andreas/Function/Registration Page/Error Invalid Email Format'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}