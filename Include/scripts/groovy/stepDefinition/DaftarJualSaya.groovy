package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.WebMobileDriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class DaftarJualSaya {
	@Given("User successfully logged in")
	public void user_successfully_logged_in() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Login/User Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User go to Daftar Jual Saya page")
	public void user_go_to_Daftar_Jual_Saya_page() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Jual Saya/Navigate to Daftar Jual Saya Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("The user can see their published products")
	public void the_user_can_see_their_published_products() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Jual Saya/Produk - Verify Product Visible'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@Given("User log in using an account that have a product transaction")
	public void user_log_in_using_an_account_that_have_a_product_transaction() {
		Mobile.startApplication('Apk/app-release-second-hand-gcp.apk', true)
		WebUI.callTestCase(findTestCase('Samuel/Pages/Login/User Login 2'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User click on Diminati tab")
	public void user_click_on_Diminati_tab() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Jual Saya/Click on Diminati'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User can see the products that another user have bidded")
	public void user_can_see_the_products_that_another_user_have_bidded() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Jual Saya/Diminati - Verify Interested Product Visible'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@When("User click on Terjual tab")
	public void user_click_on_Terjual_tab() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Jual Saya/Click on Terjual'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User can see the products that have sold")
	public void user_can_see_the_products_that_have_sold() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Jual Saya/Terjual - Verify Sold Product Visible'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@Given("User logged in using an account that user have not published any product yet")
	public void user_logged_in_using_an_account_that_user_have_not_published_any_product_yet() {
		Mobile.startApplication('Apk/app-release-second-hand-gcp.apk', true)
		WebUI.callTestCase(findTestCase('Samuel/Pages/Login/User Login 3 (empty products)'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("No products to be shown on every page in daftar jual saya page")
	public void no_products_to_be_shown_on_every_page_in_daftar_jual_saya_page() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Jual Saya/Produk - Verify No Product to be shown'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Jual Saya/Click on Diminati'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Jual Saya/Diminati - Verify No Interested Product to be Shown'), [:],
		FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Jual Saya/Click on Terjual'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Samuel/Pages/Daftar Jual Saya/Terjual - Verify No Sold Product to be Shown'), [:], FailureHandling.STOP_ON_FAILURE)

		Mobile.closeApplication()
	}
}
