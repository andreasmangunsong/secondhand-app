package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.WebMobileDriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class UpdateProduct {
	@Given("user successfully log in to the app")
	public void user_successfully_log_in_to_the_app() {
		Mobile.startApplication('Apk/app-release-second-hand-gcp.apk', true)
		WebUI.callTestCase(findTestCase('Samuel/Pages/Login/User Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("user navigated to product detail view")
	public void user_navigated_to_product_detail_view() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Navigate to Products'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("user input product name with {string}")
	public void user_input_product_name_with(String string) {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Input Product Name'), [('namaProduk') : GlobalVariable.editNamaProduk],
			FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("user input product price with {string}")
	public void user_input_product_price_with(String string) {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Input Product Price'), [('hargaProduk') : GlobalVariable.editHargaProduk],
			FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("user input product category")
	public void user_input_product_category() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Input Product Category'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("user input location")
	public void user_input_location() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Input Product Location'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("user input product description")
	public void user_input_product_description() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Input Product Deskripsi'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("user input product image")
	public void user_input_product_image() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Input Product Image'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	

	@When("user click button perbarui")
	public void user_click_button_perbarui() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Click Perbarui'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("Product Updated and user directed to product view")
	public void product_Updated_and_user_directed_to_product_view() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Verify Product Updated'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}
	

	@When("user clear text on product name")
	public void user_clear_text_on_product_name() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Empty Data/Clear Text Nama'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("user clear text on product price")
	public void user_clear_text_on_product_price() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Empty Data/Clear Text Harga'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("user clear selected category")
	public void user_clear_selected_category() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Empty Data/Clear Selected Category'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("User clear text on location")
	public void user_clear_text_on_location() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Empty Data/Clear Text Location'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("user clear text on product description")
	public void user_clear_text_on_product_description() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Empty Data/Clear Text Deskripsi'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("error message will appear to validate empty fields")
	public void error_message_will_appear_to_validate_empty_fields() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Update Product/Empty Data/Verify Empty Data'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}
}
