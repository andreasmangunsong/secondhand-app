package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.WebMobileDriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class Delete {
	@Given("User logged in and Create a Product")
	public void user_logged_in_and_Create_a_Product() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Login/User Login'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Samuel/Pages/Delete Product/Add Product to be Deleted'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user automatically navigated to daftar jual saya page")
	public void user_automatically_navigated_to_daftar_jual_saya_page() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Delete Product/Verify Daftar Jual Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Click on delete Product")
	public void user_Click_on_delete_Product() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Delete Product/Delete a Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Product successfully deleted")
	public void Product_successfully_deleted() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Delete Product/Verify Product Deleted'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
