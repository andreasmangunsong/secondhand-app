package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class EditProfile {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("user launch Binar Secondhand App")
	public void user_launch_Binar_Secondhand_App() {
		WebUI.callTestCase(findTestCase('Kastori/Launch App'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User login into the account")
	public void User_login_into_the_account() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click on Edit Profile button")
	public void User_click_Edit_on_Profile_button() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Click Edit Profile'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User click edit profile without update data")
	public void User_click_edit_profile_without_update_data() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Click Without Update and Save'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User edit profile")
	public void User_edit_profile() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Edit Profile With -'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User blank one of the field")
	public void User_blank_one_of_the_field() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Edit Profile With Blank Field'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User get success verification pop up")
	public void User_get_success_verification_pop_up() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Verify Success Edit Profile'), [:], FailureHandling.STOP_ON_FAILURE)
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Logout'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User get Error message")
	public void User_get_Error_message() {
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Verify Blank Filed'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.pressBack()
		WebUI.callTestCase(findTestCase('Kastori/Function/Edit Profile/Logout'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
