package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class LoginApp {
	@Given("user click tab Akun")
	public void user_click_tab_Akun() {
		Mobile.startApplication('Apk/app-release-second-hand-gcp.apk', true)
		Mobile.callTestCase(findTestCase('Andreas/Function/Login Page/Click Tab Akun'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("user click button Masuk di tab Akun")
	public void user_click_button_Masuk_di_tab_Akun() {
		Mobile.callTestCase(findTestCase('Andreas/Function/Login Page/Click Button Masuk Akun'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("user input email {string}")
	public void user_input_email(String email) {
		Mobile.callTestCase(findTestCase('Andreas/Function/Login Page/Input Email'), [('email') : GlobalVariable.email_def], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("user input password {string}")
	public void user_input_password(String password) {
		Mobile.callTestCase(findTestCase('Andreas/Function/Login Page/Input Password'), [('password') : GlobalVariable.password_def],
		FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user click button Masuk di tab Login")
	public void user_click_button_Masuk_di_tab_Login() {
		Mobile.callTestCase(findTestCase('Andreas/Function/Login Page/Click Button Masuk Login'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user will redirected to Akun Saya")
	public void user_will_redirected_to_Akun_Saya() {
		Mobile.callTestCase(findTestCase('Andreas/Function/Login Page/Verify Login'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@Then("error message appear")
	public void error_message_appear() {
		Mobile.callTestCase(findTestCase('Andreas/Function/Login Page/Error EmptyEmail'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@Then("user will failed to login")
	public void user_will_failed_to_login() {
		Mobile.callTestCase(findTestCase('Andreas/Function/Login Page/Error Wrong EmailPassword'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@When("user input incorrect email {string}")
	public void user_input_incorrect_email(String email) {
		Mobile.callTestCase(findTestCase('Andreas/Function/Login Page/Input Email'), [('email') : email], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user input incorrect password {string}")
	public void user_input_incorrect_password(String password) {
		Mobile.callTestCase(findTestCase('Andreas/Function/Login Page/Input Password'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}
}