package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class AddProduct {
	@Given("User successfully log in to the web")
	public void user_successfully_log_in_to_the_web() {
		Mobile.startApplication('Apk/app-release-second-hand-gcp.apk', true)
		WebUI.callTestCase(findTestCase('Samuel/Pages/Login/User Login 4'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Add a Product")
	public void user_Add_a_Product() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Add Product/Add Product'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will see their product successfully published")
	public void user_will_see_their_product_successfully_published() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Add Product/Verify Product Published'), [:], FailureHandling.STOP_ON_FAILURE)
		Mobile.closeApplication()
	}

	@When("User wants to add a product without enter product name")
	public void user_wants_to_add_a_product_without_enter_product_name() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Add Product/Set Empty Name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User will see an error message appear to validate name field")
	public void user_will_se_an_error_message_appear_to_validate_name_field() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Add Product/Verify Error Message'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("App successfully opened")
	public void app_successfully_opened() {
		Mobile.startApplication('Apk/app-release-second-hand-gcp.apk', true)
	}

	@When("User click Add Product button")
	public void user_click_Add_Product_button() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Add Product/Click Button Add'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User will be asked to log in")
	public void user_will_be_asked_to_log_in() {
		WebUI.callTestCase(findTestCase('Samuel/Pages/Add Product/Verify User have to login first'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}