$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/RegisterApp.feature");
formatter.feature({
  "name": "Register",
  "description": "  As a user, I want to register to secondhand web",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@Smoke"
    }
  ]
});
formatter.scenario({
  "name": "REG01 - User want to register, but all fields are empty",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@Smoke"
    },
    {
      "name": "@RegisterInValid"
    },
    {
      "name": "@REG01"
    }
  ]
});
formatter.step({
  "name": "user click tab Akun",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginApp.user_click_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button Masuk di tab Akun",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_click_button_Masuk_di_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click link daftar",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_click_link_daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user scroll to button daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "RegisterApp.user_scroll_to_button_daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button daftar",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_click_button_daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "error message appear empty field",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.error_message_appear_empty_field()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG02 - User want to register using incorrect email format",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@Smoke"
    },
    {
      "name": "@RegisterInValid"
    },
    {
      "name": "@REG02"
    }
  ]
});
formatter.step({
  "name": "user click tab Akun",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginApp.user_click_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button Masuk di tab Akun",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_click_button_Masuk_di_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click link daftar",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_click_link_daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input Nama \"andreas\"",
  "keyword": "When "
});
formatter.match({
  "location": "RegisterApp.user_input_Nama(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input incorrect email format \"andreasmail\"",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_input_incorrect_email_format(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input password \"binar0202\"",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_input_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input nomor HP \"0876416843\"",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_input_nomor_HP(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input kota \"Jogja\"",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_input_kota(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input alamat \"Jl. i aja dulu\"",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_input_alamat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user scroll to button daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "RegisterApp.user_scroll_to_button_daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button daftar",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_click_button_daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "error message appear invalid email",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.error_message_appear_invalid_email()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "REG03 - User want to register using correct credential",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Register"
    },
    {
      "name": "@Smoke"
    },
    {
      "name": "@RegisterValid"
    },
    {
      "name": "@REG03"
    }
  ]
});
formatter.step({
  "name": "user click tab Akun",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginApp.user_click_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button Masuk di tab Akun",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_click_button_Masuk_di_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click link daftar",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_click_link_daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input Nama \"andreas\"",
  "keyword": "When "
});
formatter.match({
  "location": "RegisterApp.user_input_Nama(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input random email \"randomEmail\"",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_input_random_email(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input password \"binar0202\"",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_input_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input nomor HP \"0876416843\"",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_input_nomor_HP(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input kota \"Jogja\"",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_input_kota(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input alamat \"Jl. i aja dulu\"",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_input_alamat(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user scroll to button daftar",
  "keyword": "Then "
});
formatter.match({
  "location": "RegisterApp.user_scroll_to_button_daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button daftar",
  "keyword": "And "
});
formatter.match({
  "location": "RegisterApp.user_click_button_daftar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will redirected to Akun Saya",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_will_redirected_to_Akun_Saya()"
});
formatter.result({
  "status": "passed"
});
});