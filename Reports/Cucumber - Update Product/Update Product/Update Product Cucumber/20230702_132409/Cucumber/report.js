$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/UpdateProduct.feature");
formatter.feature({
  "name": "Update Product",
  "description": "\tUser wants to edit a product they have published before",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "UPD01 - User wants Update their Product with valid data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@UpdateProduct"
    },
    {
      "name": "@UPD01"
    },
    {
      "name": "@positive"
    }
  ]
});
formatter.step({
  "name": "user successfully log in to the app",
  "keyword": "Given "
});
formatter.match({
  "location": "UpdateProduct.user_successfully_log_in_to_the_app()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user navigated to product detail view",
  "keyword": "When "
});
formatter.match({
  "location": "UpdateProduct.user_navigated_to_product_detail_view()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product name with \"Test Edittt\"",
  "keyword": "When "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_name_with(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product price with \"456000\"",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_price_with(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product category",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input location",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_location()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product description",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_description()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product image",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_input_product_image()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button perbarui",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_click_button_perbarui()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Product Updated and user directed to product view",
  "keyword": "Then "
});
formatter.match({
  "location": "UpdateProduct.product_Updated_and_user_directed_to_product_view()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "UPD02 - User wants to update without enter any data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@UpdateProduct"
    },
    {
      "name": "@UPD02"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "user successfully log in to the app",
  "keyword": "Given "
});
formatter.match({
  "location": "UpdateProduct.user_successfully_log_in_to_the_app()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user navigated to product detail view",
  "keyword": "When "
});
formatter.match({
  "location": "UpdateProduct.user_navigated_to_product_detail_view()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clear text on product name",
  "keyword": "When "
});
formatter.match({
  "location": "UpdateProduct.user_clear_text_on_product_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clear text on product price",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_clear_text_on_product_price()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clear selected category",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_clear_selected_category()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clear text on location",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_clear_text_on_location()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clear text on product description",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_clear_text_on_product_description()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button perbarui",
  "keyword": "And "
});
formatter.match({
  "location": "UpdateProduct.user_click_button_perbarui()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "error message will appear to validate empty fields",
  "keyword": "Then "
});
formatter.match({
  "location": "UpdateProduct.error_message_will_appear_to_validate_empty_fields()"
});
formatter.result({
  "status": "passed"
});
});