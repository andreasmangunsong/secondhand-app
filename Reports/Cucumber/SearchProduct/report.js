$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/SearchProduct.feature");
formatter.feature({
  "name": "Search Product",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@SearchProduct"
    }
  ]
});
formatter.scenario({
  "name": "SP01 - User search category Elektronik",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SearchProduct"
    },
    {
      "name": "@SP01"
    }
  ]
});
formatter.step({
  "name": "user launch Secondhand App",
  "keyword": "Given "
});
formatter.match({
  "location": "SearchProduct.user_launch_Secondhand_App()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user tap on Elektronik category button",
  "keyword": "When "
});
formatter.match({
  "location": "SearchProduct.user_tap_on_Elektronik_category_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will redirected to product page with Elektronik category",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchProduct.user_will_redirected_to_product_page_with_Elektronik_category()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "SP02 - User search category Komputer dan Aksesoris",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SearchProduct"
    },
    {
      "name": "@SP02"
    }
  ]
});
formatter.step({
  "name": "user launch Secondhand App",
  "keyword": "Given "
});
formatter.match({
  "location": "SearchProduct.user_launch_Secondhand_App()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user tap on Komputer dan Aksesoris category button",
  "keyword": "When "
});
formatter.match({
  "location": "SearchProduct.user_tap_on_Komputer_dan_Aksesoris_category_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will redirected to product page with Komputer dan Aksesoris category",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchProduct.user_will_redirected_to_product_page_with_Komputer_dan_Aksesoris_category()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "SP03 - User search All category product",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SearchProduct"
    },
    {
      "name": "@SP03"
    }
  ]
});
formatter.step({
  "name": "user launch Secondhand App",
  "keyword": "Given "
});
formatter.match({
  "location": "SearchProduct.user_launch_Secondhand_App()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user tap on Elektronik category button",
  "keyword": "When "
});
formatter.match({
  "location": "SearchProduct.user_tap_on_Elektronik_category_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user tap on All category button",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchProduct.user_tap_on_All_category_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will redirected to product page with All category",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchProduct.user_will_redirected_to_product_page_with_All_category()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "SP04 - User search by keyword",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@SearchProduct"
    },
    {
      "name": "@SP04"
    }
  ]
});
formatter.step({
  "name": "user launch Secondhand App",
  "keyword": "Given "
});
formatter.match({
  "location": "SearchProduct.user_launch_Secondhand_App()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user search baju in search product",
  "keyword": "When "
});
formatter.match({
  "location": "SearchProduct.user_search_baju_in_search_product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will redirected to product page with baju keyword",
  "keyword": "Then "
});
formatter.match({
  "location": "SearchProduct.user_will_redirected_to_product_page_with_baju_keyword()"
});
formatter.result({
  "status": "passed"
});
});