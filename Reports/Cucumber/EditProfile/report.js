$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/EditProfile.feature");
formatter.feature({
  "name": "Edit Profile",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@EditProfile"
    }
  ]
});
formatter.scenario({
  "name": "EP01 - Edit Profile Without Update Data",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EP01"
    }
  ]
});
formatter.step({
  "name": "user launch Binar Secondhand App",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.user_launch_Binar_Secondhand_App()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User login into the account",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.User_login_into_the_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Edit Profile button",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_click_Edit_on_Profile_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click edit profile without update data",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_click_edit_profile_without_update_data()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User get success verification pop up",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_get_success_verification_pop_up()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EP02 - User edit profile",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EP02"
    }
  ]
});
formatter.step({
  "name": "user launch Binar Secondhand App",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.user_launch_Binar_Secondhand_App()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User login into the account",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.User_login_into_the_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Edit Profile button",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_click_Edit_on_Profile_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User edit profile",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_edit_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User get success verification pop up",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_get_success_verification_pop_up()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "EP03 - User update profile with one blank field",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@EditProfile"
    },
    {
      "name": "@EP03"
    }
  ]
});
formatter.step({
  "name": "user launch Binar Secondhand App",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProfile.user_launch_Binar_Secondhand_App()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User login into the account",
  "keyword": "When "
});
formatter.match({
  "location": "EditProfile.User_login_into_the_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Edit Profile button",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_click_Edit_on_Profile_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User blank one of the field",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_blank_one_of_the_field()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User get Error message",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProfile.User_get_Error_message()"
});
formatter.result({
  "status": "passed"
});
});