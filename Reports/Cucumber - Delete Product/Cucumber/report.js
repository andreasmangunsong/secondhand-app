$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/Delete.feature");
formatter.feature({
  "name": "Delete Product",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@delete"
    }
  ]
});
formatter.scenario({
  "name": "User wants to delete a product",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@delete"
    },
    {
      "name": "@DEL01"
    }
  ]
});
formatter.step({
  "name": "User logged in and Create a Product",
  "keyword": "Given "
});
formatter.match({
  "location": "Delete.user_logged_in_and_Create_a_Product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user automatically navigated to daftar jual saya page",
  "keyword": "When "
});
formatter.match({
  "location": "Delete.user_automatically_navigated_to_daftar_jual_saya_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click on delete Product",
  "keyword": "And "
});
formatter.match({
  "location": "Delete.user_Click_on_delete_Product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Product successfully deleted",
  "keyword": "Then "
});
formatter.match({
  "location": "Delete.Product_successfully_deleted()"
});
formatter.result({
  "status": "passed"
});
});