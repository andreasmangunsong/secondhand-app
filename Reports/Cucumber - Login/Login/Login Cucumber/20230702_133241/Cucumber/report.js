$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/LoginApp.feature");
formatter.feature({
  "name": "Login",
  "description": "  As a user, I want to login to Secondhand app",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@Smoke"
    }
  ]
});
formatter.scenario({
  "name": "LOG01 - User want to login without input email and password",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@Smoke"
    },
    {
      "name": "@loginInvalid"
    },
    {
      "name": "@LOG01"
    }
  ]
});
formatter.step({
  "name": "user click tab Akun",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginApp.user_click_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button Masuk di tab Akun",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_click_button_Masuk_di_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button Masuk di tab Login",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginApp.user_click_button_Masuk_di_tab_Login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "error message appear",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.error_message_appear()"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "LOG02 - User want to login using incorrect credential",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@loginInvalid"
    },
    {
      "name": "@LOG02"
    }
  ]
});
formatter.step({
  "name": "user click tab Akun",
  "keyword": "Given "
});
formatter.step({
  "name": "user click button Masuk di tab Akun",
  "keyword": "And "
});
formatter.step({
  "name": "user input incorrect email \"\u003cemail\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "user input incorrect password \"\u003cpassword\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user click button Masuk di tab Login",
  "keyword": "Then "
});
formatter.step({
  "name": "user will failed to login",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "email",
        "password"
      ]
    },
    {
      "cells": [
        "binarteam2@gmail.com",
        "wrongPass"
      ]
    },
    {
      "cells": [
        "wrongEmail@gmail.com",
        "binar0202"
      ]
    },
    {
      "cells": [
        "wrongEmail@gmail.com",
        "wrongPass"
      ]
    }
  ]
});
formatter.scenario({
  "name": "LOG02 - User want to login using incorrect credential",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@Smoke"
    },
    {
      "name": "@loginInvalid"
    },
    {
      "name": "@LOG02"
    }
  ]
});
formatter.step({
  "name": "user click tab Akun",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginApp.user_click_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button Masuk di tab Akun",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_click_button_Masuk_di_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input incorrect email \"binarteam2@gmail.com\"",
  "keyword": "When "
});
formatter.match({
  "location": "LoginApp.user_input_incorrect_email(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input incorrect password \"wrongPass\"",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_input_incorrect_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button Masuk di tab Login",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginApp.user_click_button_Masuk_di_tab_Login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will failed to login",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_will_failed_to_login()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LOG02 - User want to login using incorrect credential",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@Smoke"
    },
    {
      "name": "@loginInvalid"
    },
    {
      "name": "@LOG02"
    }
  ]
});
formatter.step({
  "name": "user click tab Akun",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginApp.user_click_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button Masuk di tab Akun",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_click_button_Masuk_di_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input incorrect email \"wrongEmail@gmail.com\"",
  "keyword": "When "
});
formatter.match({
  "location": "LoginApp.user_input_incorrect_email(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input incorrect password \"binar0202\"",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_input_incorrect_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button Masuk di tab Login",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginApp.user_click_button_Masuk_di_tab_Login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will failed to login",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_will_failed_to_login()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LOG02 - User want to login using incorrect credential",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@Smoke"
    },
    {
      "name": "@loginInvalid"
    },
    {
      "name": "@LOG02"
    }
  ]
});
formatter.step({
  "name": "user click tab Akun",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginApp.user_click_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button Masuk di tab Akun",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_click_button_Masuk_di_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input incorrect email \"wrongEmail@gmail.com\"",
  "keyword": "When "
});
formatter.match({
  "location": "LoginApp.user_input_incorrect_email(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input incorrect password \"wrongPass\"",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_input_incorrect_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button Masuk di tab Login",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginApp.user_click_button_Masuk_di_tab_Login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will failed to login",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_will_failed_to_login()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "LOG03 - User want to login using correct credential",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Login"
    },
    {
      "name": "@Smoke"
    },
    {
      "name": "@loginValid"
    },
    {
      "name": "@LOG03"
    }
  ]
});
formatter.step({
  "name": "user click tab Akun",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginApp.user_click_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button Masuk di tab Akun",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_click_button_Masuk_di_tab_Akun()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input email \"binarteam2@gmail.com\"",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_input_email(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input password \"binar0202\"",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_input_password(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button Masuk di tab Login",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginApp.user_click_button_Masuk_di_tab_Login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will redirected to Akun Saya",
  "keyword": "And "
});
formatter.match({
  "location": "LoginApp.user_will_redirected_to_Akun_Saya()"
});
formatter.result({
  "status": "passed"
});
});