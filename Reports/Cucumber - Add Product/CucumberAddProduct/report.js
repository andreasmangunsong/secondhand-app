$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/AddProduct.feature");
formatter.feature({
  "name": "Add Product",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@AddProduct"
    }
  ]
});
formatter.scenario({
  "name": "ADP01 - User wants to add a product",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@positive"
    },
    {
      "name": "@ADP01"
    }
  ]
});
formatter.step({
  "name": "User successfully log in to the web",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProduct.user_successfully_log_in_to_the_web()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Add a Product",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_Add_a_Product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see their product successfully published",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_will_see_their_product_successfully_published()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "ADP02 - User wants to add product without enter products name",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@negative"
    },
    {
      "name": "@ADP02"
    }
  ]
});
formatter.step({
  "name": "User successfully log in to the web",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProduct.user_successfully_log_in_to_the_web()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Add Product button",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_click_Add_Product_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User wants to add a product without enter product name",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_wants_to_add_a_product_without_enter_product_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will see an error message appear to validate name field",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_will_se_an_error_message_appear_to_validate_name_field()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "ADP03 - User wants to add product but did not log in to the app",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@AddProduct"
    },
    {
      "name": "@negative"
    },
    {
      "name": "@ADP03"
    }
  ]
});
formatter.step({
  "name": "App successfully opened",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProduct.app_successfully_opened()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click Add Product button",
  "keyword": "When "
});
formatter.match({
  "location": "AddProduct.user_click_Add_Product_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will be asked to log in",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProduct.user_will_be_asked_to_log_in()"
});
formatter.result({
  "status": "passed"
});
});