$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/DaftarTransaksi.feature");
formatter.feature({
  "name": "Daftar Transaksi",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@DaftarTransaksi"
    }
  ]
});
formatter.scenario({
  "name": "DAF01 - Users wants to see transaction of their products",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@DaftarTransaksi"
    },
    {
      "name": "@DAF01"
    }
  ]
});
formatter.step({
  "name": "User login using an account that have product transaction",
  "keyword": "Given "
});
formatter.match({
  "location": "DaftarTransaksi.user_login_using_an_account_that_have_product_transaction()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Transaksi Menu",
  "keyword": "When "
});
formatter.match({
  "location": "DaftarTransaksi.user_click_on_Transaksi_Menu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will be shown the products which have an interaction or transaction",
  "keyword": "Then "
});
formatter.match({
  "location": "DaftarTransaksi.user_will_be_shown_the_products_which_have_an_interaction_or_transaction()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "DAF02 - Users does not have a product interaction or transaction",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@DaftarTransaksi"
    },
    {
      "name": "@DAF02"
    }
  ]
});
formatter.step({
  "name": "User login using an account that does not have a transaction",
  "keyword": "Given "
});
formatter.match({
  "location": "DaftarTransaksi.user_login_using_an_account_that_does_not_have_a_transaction()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Transaksi Menu",
  "keyword": "When "
});
formatter.match({
  "location": "DaftarTransaksi.user_click_on_Transaksi_Menu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will be shown message that tells no product interaction or transaction at the moment",
  "keyword": "Then "
});
formatter.match({
  "location": "DaftarTransaksi.user_will_be_shown_message_that_tells_no_product_interaction_or_transaction_at_the_moment()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "DAF03 - User open Transaction menu but did not log in to the app",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@DaftarTransaksi"
    },
    {
      "name": "@DAF03"
    }
  ]
});
formatter.step({
  "name": "Application Susccesfully opened, but user have not log in to the app",
  "keyword": "Given "
});
formatter.match({
  "location": "DaftarTransaksi.application_Susccesfully_opened_but_user_have_not_log_in_to_the_app()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Transaksi Menu",
  "keyword": "When "
});
formatter.match({
  "location": "DaftarTransaksi.user_click_on_Transaksi_Menu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User will get a message that ask them to log in",
  "keyword": "Then "
});
formatter.match({
  "location": "DaftarTransaksi.user_will_get_a_message_that_ask_them_to_log_in()"
});
formatter.result({
  "status": "passed"
});
});