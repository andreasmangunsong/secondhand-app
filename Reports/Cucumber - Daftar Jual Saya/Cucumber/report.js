$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/DaftarJualSaya.feature");
formatter.feature({
  "name": "Daftar Jual Saya",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@DaftarJualSaya"
    }
  ]
});
formatter.scenario({
  "name": "DJS01 - User wants to see all products they have published",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@DaftarJualSaya"
    },
    {
      "name": "@DJS01"
    },
    {
      "name": "@positive"
    }
  ]
});
formatter.step({
  "name": "User successfully logged in",
  "keyword": "Given "
});
formatter.match({
  "location": "DaftarJualSaya.user_successfully_logged_in()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User go to Daftar Jual Saya page",
  "keyword": "When "
});
formatter.match({
  "location": "DaftarJualSaya.user_go_to_Daftar_Jual_Saya_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The user can see their published products",
  "keyword": "Then "
});
formatter.match({
  "location": "DaftarJualSaya.the_user_can_see_their_published_products()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "DJS02 - User wants to see products which buyer interested with",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@DaftarJualSaya"
    },
    {
      "name": "@DJS02"
    },
    {
      "name": "@positive"
    }
  ]
});
formatter.step({
  "name": "User log in using an account that have a product transaction",
  "keyword": "Given "
});
formatter.match({
  "location": "DaftarJualSaya.user_log_in_using_an_account_that_have_a_product_transaction()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User go to Daftar Jual Saya page",
  "keyword": "When "
});
formatter.match({
  "location": "DaftarJualSaya.user_go_to_Daftar_Jual_Saya_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Diminati tab",
  "keyword": "And "
});
formatter.match({
  "location": "DaftarJualSaya.user_click_on_Diminati_tab()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can see the products that another user have bidded",
  "keyword": "Then "
});
formatter.match({
  "location": "DaftarJualSaya.user_can_see_the_products_that_another_user_have_bidded()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "DJS03 - User wants to see their sold products",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@DaftarJualSaya"
    },
    {
      "name": "@DJS03"
    },
    {
      "name": "@positive"
    }
  ]
});
formatter.step({
  "name": "User log in using an account that have a product transaction",
  "keyword": "Given "
});
formatter.match({
  "location": "DaftarJualSaya.user_log_in_using_an_account_that_have_a_product_transaction()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User go to Daftar Jual Saya page",
  "keyword": "When "
});
formatter.match({
  "location": "DaftarJualSaya.user_go_to_Daftar_Jual_Saya_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on Terjual tab",
  "keyword": "And "
});
formatter.match({
  "location": "DaftarJualSaya.user_click_on_Terjual_tab()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User can see the products that have sold",
  "keyword": "Then "
});
formatter.match({
  "location": "DaftarJualSaya.user_can_see_the_products_that_have_sold()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "DJS04 - User wants to see their products, although they did not published any product before",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@DaftarJualSaya"
    },
    {
      "name": "@DJS04"
    },
    {
      "name": "@negative"
    }
  ]
});
formatter.step({
  "name": "User logged in using an account that user have not published any product yet",
  "keyword": "Given "
});
formatter.match({
  "location": "DaftarJualSaya.user_logged_in_using_an_account_that_user_have_not_published_any_product_yet()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User go to Daftar Jual Saya page",
  "keyword": "When "
});
formatter.match({
  "location": "DaftarJualSaya.user_go_to_Daftar_Jual_Saya_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "No products to be shown on every page in daftar jual saya page",
  "keyword": "Then "
});
formatter.match({
  "location": "DaftarJualSaya.no_products_to_be_shown_on_every_page_in_daftar_jual_saya_page()"
});
formatter.result({
  "status": "passed"
});
});